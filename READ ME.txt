For this project I wanted to look at some of the unity communitie's tree designs. I ended up settling on looking just
at the low poly trees and how they work together in a shared environment. To encourage exploration of the space I add
changable tree colorings/texturing and trash (Highlighted to guid the player about the space) to pickup. The color 
changing trees also further promotes exploration of how the different tree styles and also their colors work together
in a shared environment.

The game controls are as follows:
WASD for directional controls.
Mouse movement for look controls.
Left click on trees and litter when promted to change or remove them.
Space bar jumps.

SourceTree/Bitbucket used for version control. Updated at the end of work sessions/when something significant was
added/changed. It came in handy once as a asset I downloaded force an update on some APIs (I think) which cause the
base project scripts to compile incorrectly.
https://bitbucket.org/OldSpice/assignment-1-gds3

Free Trees --- Darth_Artisan
https://assetstore.unity.com/packages/3d/vegetation/trees/free-trees-103208

Low Poly Style - Tree Pack --- Agile Reaction
https://assetstore.unity.com/packages/3d/vegetation/trees/low-poly-style-tree-pack-76495

Stylized Trees (Low Poly) --- Tom Poon
https://assetstore.unity.com/packages/3d/vegetation/trees/stylized-trees-low-poly-49916

Low Poly Styled Trees. --- Daniel Robnik
https://assetstore.unity.com/packages/3d/vegetation/trees/low-poly-styled-trees-43103
Low poly styled rocks
https://assetstore.unity.com/packages/3d/props/exterior/low-poly-styled-rocks-43486

Snowy Low-Poly Trees --- False Wisp Studios
https://assetstore.unity.com/packages/3d/vegetation/trees/snowy-low-poly-trees-76796

Low Poly Environment Pack --- Tons of Hun Studios
https://assetstore.unity.com/packages/3d/low-poly-environment-pack-62304

Low-Poly Park --- Thunderent's Assets
https://assetstore.unity.com/packages/3d/environments/urban/low-poly-park-61922

Free Low Poly Toon Nature --- Rare Gardener
https://assetstore.unity.com/packages/3d/environments/fantasy/free-low-poly-toon-nature-76545

Roman City Low Poly Pack 1 - Lite --- Terra Nova Creations
https://assetstore.unity.com/packages/3d/environments/roman-city-low-poly-pack-1-lite-92731

Low Poly Nature Assets Sample --- PolyLabs LLC
https://assetstore.unity.com/packages/3d/environments/low-poly-nature-assets-sample-67201

Trash Low Poly Cartoon Pack --- BlankfaceStanislav
https://assetstore.unity.com/packages/3d/trash-low-poly-cartoon-pack-66229

Low Poly: Foliage --- RAD-CODERS
https://assetstore.unity.com/packages/3d/vegetation/low-poly-foliage-66638

Free Skybox - Cubemap Extended --- BOXOPHOBIC
https://assetstore.unity.com/packages/vfx/shaders/free-skybox-cubemap-extended-107400

Low Poly Survival Essentials --- Broken Vector
https://assetstore.unity.com/packages/3d/props/tools/low-poly-survival-essentials-109444
Low Poly Shaders 
https://assetstore.unity.com/packages/vfx/shaders/low-poly-shaders-85262

Cube Space - Effects (Free Version) --- Swift Games
https://assetstore.unity.com/packages/vfx/particles/cube-space-effects-74293

Low Polygon Soccer Ball --- Ahmet Gencoglu
https://assetstore.unity.com/packages/3d/low-polygon-soccer-ball-84382

semi transparent outline --- ZhangGameMaker
https://assetstore.unity.com/packages/vfx/shaders/semi-transparent-outline-86481